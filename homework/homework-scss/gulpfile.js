const gulp = require('gulp');
const sass = require('gulp-sass');
const del = require('del');
const rename = require('gulp-rename');
const prefixes = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const cleanCss = require('gulp-clean-css');
const minImage = require('gulp-imagemin');
const minJs = require('gulp-js-minify');
const browserSync = require('browser-sync').create();

function build() {
    gulp.src('./index.html')
        .pipe(gulp.dest('./built/'));
}

function emptyDist() {
    return del('./dist/**')
}

function cleanBuild() {
    return del('./built');
}

function copyHtml() {
    return src('./src/index.html')
        .pipe(gulp.dest('./dist'))
}

function reloadOnChange() {
    browserSync.init({
        server: './built'
    });
    gulp.watch(['./page.html']).on('change', gulp.series(build, browserSync.reload));
}

function buildScss() {
    gulp.src("./scss/style.scss")
        .pipe(sass({
            errorLogToConsole: true,
            outputStyle: "compressed"
        }))
        .on("error", console.error.bind(console))
        .pipe(prefixes({
            cascade: false
        }))
        .pipe(gulp.dest("./css/"))
        .pipe(concat('all.css'))
        .pipe(browserSync.stream());
}

function copyImg() {
    return src('./src/img/**')
        .pipe(minImage('/*.img'))
        .pipe(gulp.dest('./dist/img'))

}

function copyJs() {
    return src('./src/js/script.js')
        .pipe(minJs('/*.js'))
        .pipe(gulp.dest('./dist/js'))
}

exports.html = copyHtml;
exports.scss = buildScss;
exports.images = copyImg;
exports.clear = emptyDist;
exports.js = copyJs;
exports.build = gulp.series(emptyDist,
    gulp.parallel(gulp.series(copyHtml,buildScss),
        copyImg, copyJs));

exports.default = () => src ("style.scss")
    .pipe(sass())
    .pipe(gulp.dest("./dist"));