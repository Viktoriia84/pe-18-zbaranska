class Column {
    constructor(parent, number){
        this.parent = parent;
        this.cardNumber = number;
        this.cards = [];
        this.createColumn(this.parent);
        this.createnewCardListener();
    }

    createColumn(parent){
        this.column = document.createElement('div');
        this.column.classList.add('column');

        this.columnHeader = document.createElement('div');
        this.columnHeader.classList.add('column-header');

        this.columnFooter = document.createElement('div');
        this.columnFooter.classList.add('column-footer');

        this.createNewCardBtn = document.createElement('button');
        this.createNewCardBtn.classList.add('create-card-btn');
        this.createNewCardBtn.innerText ='Create a new card';
        this.columnFooter.append(this.createNewCardBtn);

        this.cardsBody = document.createElement('div');
        this.cardsBody.classList.add('cards-body');

        this.sortBtn = document.createElement('button');
        this.sortBtn.innerText = 'Sort by alphabet';
        this.sortBtn.classList.add('sort-btn');
        this.columnHeader.append(this.sortBtn);

        this.column.append(this.columnHeader, this.cardsBody, this.columnFooter);
        parent.prepend(this.column);
    }

    createNewCard(){
        this.cards.push(new Card(this.cardsBody, this.cardNumber, this));
        this.cardNumber++;
    }

    createnewCardListener(){
        this.createNewCardBtn.addEventListener('click', () => {
            this.createNewCard();
        })

        this.sortBtn.addEventListener('click',()=>{
            this.cards.sort((prev, next) => {
                if ( prev.card.innerText < next.card.innerText ) return -1;
                if ( prev.card.innerText < next.card.innerText ) return 1;
            });
            this.cards.forEach(element => {
                this.cardsBody.append(element.card);
            });
        })
    }
}

class Container {
    constructor(parent) {
        this.parent = parent;
        this.columns = [];
        this.createContainer(this.parent);
        this.createNewColumnListener();
    }

    createContainer(){
        this.container = document.createElement('div');
        this.container.classList.add('container');
        this.containerHeader = document.createElement('div');
        this.containerHeader.classList.add('container-header');
        this.containerBody = document.createElement('div');
        this.containerBody.classList.add('container-body');
        this.createNewColumnBtn = document.createElement('button');
        this.createNewColumnBtn.classList.add('create-new-col-btn');
        this.createNewColumnBtn.innerText = 'Create a new column';
        this.containerHeader.append(this.createNewColumnBtn);
        this.container.append(this.containerHeader, this.containerBody);
        this.parent.append(this.container);
    }

    createNewColumn(){
        this.columns.push(new Column(this.containerBody, this.columns.length));
    }

    createNewColumnListener(){
        this.createNewColumnBtn.addEventListener('click', ()=>{
            this.createNewColumn();
        })
    }
}
window.container = new Container(document.body);

class Card {
    constructor(parentEl, number, parentObj) {
        this.parentEl = parentEl;
        this.parentObj = parentObj;
        this.number = number;
        this.createCard(this.parentEl);
        this.createCardlistener();
    }

    createCard(parentEl) {
        this.card = document.createElement('div');
        this.card.setAttribute('data-number', this.number);
        this.card.setAttribute('draggable', 'true');
        this.card.classList.add('card');
        this.cardContent = document.createElement('p');
        this.cardContent.classList.add('card-content');
        this.cardContent.innerText = ' ';
        this.cardContent.setAttribute('contenteditable', 'true');
        this.card.append(this.cardContent);
        parentEl.append(this.card);
    }

    createCardlistener = () => {
        this.parentObj.selected = null;

        let dragOver = (e) => {
            if (neighbourExists(this.parentObj.selected, e.target)) {
                e.target.parentNode.insertBefore(this.parentObj.selected, e.target)
            }else if(neighbourExists(this.parentObj.selected, e.target.parentNode)){
                e.target.closest('.cards-body').insertBefore(this.parentObj.selected, e.target.parentNode);
            }
            else{
                if(e.target.classList.contains('card')){
                    e.target.parentNode.insertBefore(this.parentObj.selected, e.target.nextSibling)
                }else  e.target.closest('.cards-body').insertBefore(this.parentObj.selected, e.target.parentNode.nextSibling);
            }
        }

        let dragEnd = () => {
            this.parentObj.selected = null
        }

        let dragStart = (e) => {
            e.dataTransfer.effectAllowed = "move";
            e.dataTransfer.setData("text/plain", null);
            this.parentObj.selected = e.target;
        }

        let neighbourExists = (card1, card2) => {
            let neighbour;
            if (card2.parentNode === card1.parentNode) {
                for (neighbour = card1.previousSibling; neighbour; neighbour = neighbour.previousSibling) {
                    if (neighbour === card2) return true;
                }
            } else return false;
        }

        this.card.addEventListener('dragstart', function () {
            dragStart(event);
        }.bind(this));
        this.card.addEventListener('dragover', function () {
            dragOver(event);
        }.bind(this));
        this.card.addEventListener('dragend', function () {
            dragEnd();
        }.bind(this));
    }
}


