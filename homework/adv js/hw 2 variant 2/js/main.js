class Hamburger {
    constructor(size, stuffing) {
        try {
            if (!size || size.type !== 'size') {
                throw new HamburgerExceptions("Size is not given or invalid size.");
            } 
            this._size = size;
           
            if (!stuffing || stuffing.type !== 'stuffing') {
                throw new HamburgerExceptions("Stuffing is not given or invalid stuffing.");
            }
            this._stuffing = stuffing;
            this._toppings = [];
        } catch (e) {
            console.log(e.message);
        }
    }

    static SIZE_SMALL = {
        name: 'small',
        price: 50,
        calories: 20,
        type: 'size'
    };
    static SIZE_LARGE = {
        name: 'large',
        price: 100,
        calories: 40,
        type: 'size'
    };
    static STUFFING_CHEESE = {
        name: 'cheese',
        price: 10,
        calories: 20,
        type: 'stuffing'
    };
    static STUFFING_SALAD = {
        name: 'salad',
        price: 20,
        calories: 5,
        type: 'stuffing'
    };
    static STUFFING_POTATO = {
        name: 'potato',
        price: 15,
        calories: 10,
        type: 'stuffing'
    };
    static TOPPING_MAYO = {
        name: 'mayo',
        price: 20,
        calories: 5,
        type: 'topping'
    };
    static TOPPING_SPICE = {
        name: 'spice',
        price: 15,
        calories: 0,
        type: 'topping'
    };

    addTopping(topping) {
        try {
            if (!topping || topping.type !== 'topping') {
                throw new HamburgerExceptions("Topping is not given or topping doesn't exist`.");
            } else if (this._toppings.includes(topping)) {
                throw new HamburgerExceptions(`Topping ${topping.name} has already been added`);
            }
            this._toppings.push(topping);
            console.log(`Topping ${topping.name} is added`);
        } catch (e) {
            console.log(e.message);
        }
    }
    removeTopping(topping) {
        return this._topping = this._toppings.filter(e => e !== topping);
    }
    getToppings() {
        return this._topping;
    }
    getSize() {
        return this._size; 
    }
    getStuffing() {
        return this._stuffing; 
    }
    calculatePrice() {
        let calcPrice = this._size.price + this._stuffing.price;
        if (this._toppings.length > 0) {
            calcPrice += this._toppings.reduce((total, num) => {
                return total + num.price;
            }, 0);
        }
        return calcPrice;
    }
    calculateCalories() {
        let calcCalories = this._size.calories + this._stuffing.calories;
        if (this._toppings.length > 0) {
            calcCalories += this._toppings.reduce((total, num) => {
                return total + num.calories;
            }, 0);
        }
        return calcCalories;
    }
}

class HamburgerExceptions {
    constructor(message) {
        this.message = message;
        console.log('HamburgerException message:', message);
    }
}

let hamburger = new Hamburger (Hamburger.SIZE_LARGE, Hamburger.STUFFING_POTATO);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
console.log(hamburger.calculateCalories());
console.log(hamburger.calculatePrice());
hamburger.addTopping(Hamburger.TOPPING_SPICE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
console.log(hamburger.calculatePrice());
hamburger.removeTopping(Hamburger.TOPPING_MAYO);
console.log(hamburger.getToppings().length);
;
