class Cell {
    constructor(y, x, row) {
      this.x = x;
      this.y = y;
      this.element = null;
      this.available = true;
      this.render(row);
    }
  
    render(row) {
      const cell = document.createElement('td');
      this.element = cell;
      cell.classList.add('cell');
      row.appendChild(cell);
    }
  
    activate() {
      this.element.classList.add('active');
    }
  
    playerClick() {
      this.element.classList.remove('active');
      this.element.classList.add('player');
    }
  
    computerClick() {
      this.element.classList.remove('active');
      this.element.classList.add('computer');
    }
  
  }
  
class Board {
    constructor(sideX, sideY, parent, playerAction) {
      this.sideX = sideX;
      this.sideY = sideY;
      this.cells = [];
      this.playerAction = playerAction;
      this.render(parent);
    }
  
    clickHandler({ target }) {
      this.playerAction(target);
    }
  
    render(parent) {
      this.board = document.createElement('table');
      this.board.classList.add('board');
      for (let y = 0; y < this.sideY; y++) {
        const row = document.createElement('tr');
        for (let x = 0; x < this.sideX; x++) {
          this.cells.push(new Cell(y, x, row));
        }
        this.board.appendChild(row);
      }
      this.board.addEventListener('click', this.clickHandler.bind(this));
      parent.appendChild(this.board);
    }
  }
  
class Controls {
    constructor(parent, startGame) {
      this.interval = 1500;
      this.modes = [['Easy', 1500], ['Medium', 1000], ['Hard', 750]];
      this.modeButtons = [];
      this.startGame = startGame;
      this.scoreBoard = null;
      this.render(parent);
    }
  
    chooseMode({ target }) {
      console.log(this);
      this.interval = target.dataset.interval
      this.modeButtons.forEach(btn =>
        btn.classList[btn === target ? 'add' : 'remove']('active')
      );
    }
  
    setScore(playerScore, computerScore) {
      this.scoreBoard.children[0].innerText = playerScore;
      this.scoreBoard.children[1].innerText = computerScore;
    }
  
    showWinner(winner) {
      this.scoreBoard.children[2].innerText = `${winner} won!`;
    }
  
    render(parent) {
      const controlsPanel = document.createElement('div');
      controlsPanel.classList.add('controls');
      this.modes.forEach(([difficulty, interval]) => {
        const button = document.createElement('button');
        button.addEventListener('click', this.chooseMode.bind(this));
        this.modeButtons.push(button);
        button.classList.add('difficulty-btn');
        if (interval === this.interval) {
          button.classList.add('active');
        }
        button.innerText = difficulty;
        button.dataset.interval = interval;
        controlsPanel.appendChild(button);
      });
      const newGameButton = document.createElement('button');
      newGameButton.innerText = 'New game';
      newGameButton.classList.add('new-game-btn');
      newGameButton.addEventListener('click', () => this.startGame(this.interval));
      controlsPanel.appendChild(newGameButton);
  
      this.scoreBoard = document.createElement('div');
      const playerScore = document.createElement('span');
      playerScore.classList.add('player-score');
      playerScore.innerText = '0';
      const computerScore = document.createElement('span');
      computerScore.classList.add('computer-score');
      computerScore.innerText = ' 0';
      const winnerMessage = document.createElement('span');
      winnerMessage.classList.add('winner-message');
      this.scoreBoard.append(playerScore, computerScore, winnerMessage);
  
      parent.append(controlsPanel, this.scoreBoard);
    }
  }
  
  class Game {
    constructor(parent) {
      this.state = {
        playerScore: 0,
        computerScore: 0,
        difficulty: null,
        winner: null,
        activeCell: null
      };
      this.parent = parent;
      this.board = new Board(10, 10, parent, this.playerAction.bind(this));
      this.controls = new Controls(parent, this.start);
    }
  
    playerAction(target) {
      if (this.state.activeCell && target === this.state.activeCell.element) {
        this.state.activeCell.playerClick();
        this.state.playerScore++;
        this.state.activeCell = null;
        this.controls.setScore(this.state.playerScore, this.state.computerScore);
        this.checkForWinner();
      }
    }
  
    start = (interval) => {
      this.board.cells.forEach(cell => {
        cell.available = true;
        cell.element.className = 'cell';
      })
      this.state.computerScore = 0;
      this.state.playerScore = 0;
      this.state.activeCell = null;
      this.stop();
      console.log(this);
      this.intervalID = setInterval(this.slip.bind(this), interval)
    }
  
    stop() {
      clearInterval(this.intervalID);
    }
  
    slip() {
      if (this.state.activeCell) {
        this.state.activeCell.computerClick();
        this.state.computerScore++;
        this.controls.setScore(this.state.playerScore, this.state.computerScore);
        if (this.checkForWinner()) return;
      }
      const cells = this.board.cells.filter(cell => cell.available)
      const idx = Math.floor(Math.random() * cells.length);
      cells[idx].available = false;
      this.state.activeCell = cells[idx];
      cells[idx].activate();
    }
  
    checkForWinner() {
      const winningScore = Math.ceil(this.board.cells.length / 2);
  
      switch (winningScore) {
        case this.state.computerScore:
          this.state.winner = 'computer';
          break;
        case this.state.playerScore:
          this.state.winner = 'player';
          break;
          default:
          return;
      }
  
      this.stop();
      this.controls.showWinner(this.state.winner);
      return true;
    }
  }
  
  document.querySelectorAll('.whackamole').forEach(parent => {
    new Game(parent);
  });