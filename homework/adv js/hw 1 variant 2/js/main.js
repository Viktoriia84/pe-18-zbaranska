function Hamburger(size, stuffing) {
    try {
        if (!size || size.type !== 'size') {
            throw new HamburgerExceptions("Size is not given or invalid size.");
        }
        this._size = size;
        if (!stuffing || stuffing.type !== 'stuffing') {
            throw new HamburgerExceptions("Stuffing is not given or invalid stuffing.");
        }
        this._stuffing = stuffing;
        this._toppings = [];
    } catch (e) {
        console.log(e.message);
    }
}

Hamburger.SIZE_SMALL = {
    name: 'small',
    price: 50,
    calories: 20,
    type: 'size'
};
Hamburger.SIZE_LARGE = {
    name: 'large',
    price: 100,
    calories: 40,
    type: 'size'
};
Hamburger.STUFFING_CHEESE = {
    name: 'cheese',
    price: 10,
    calories: 20,
    type: 'stuffing'
};
Hamburger.STUFFING_SALAD = {
    name: 'salad',
    price: 20,
    calories: 5,
    type: 'stuffing'
};
Hamburger.STUFFING_POTATO = {
    name: 'potato',
    price: 15,
    calories: 10,
    type: 'stuffing'
};
Hamburger.TOPPING_MAYO = {
    name: 'mayo',
    price: 20,
    calories: 5,
    type: 'topping'
};
Hamburger.TOPPING_SPICE = {
    name: 'spice',
    price: 15,
    calories: 0,
    type: 'topping'
};

Hamburger.prototype.addTopping = function (topping) {
    try {
        if (!topping || topping.type !== 'topping') {
            throw new HamburgerExceptions('Topping is not given or invalid topping.');
        } else if (this._toppings.includes(topping)) {
            throw new HamburgerExceptions(`Topping ${topping.name} has already been added`);
        }
        this._toppings.push(topping);
        console.log(`Topping ${topping.name} is added`);
    } catch (e) {
        console.log(e.message);
    }
};
Hamburger.prototype.removeTopping = function (toppings) {
    return this._topping = this._toppings.filter(e => e !== toppings);
};
Hamburger.prototype.getToppings = function () {
    return this._topping;
};
Hamburger.prototype.getSize = function () {
    return this._size;
};
Hamburger.prototype.getStuffing = function () {
    return this._stuffing;
};

Hamburger.prototype.calculatePrice = function () {
    let calcPrice = this._size.price + this._stuffing.price;
    if (this._toppings.length > 0) {
        calcPrice += this._toppings.reduce((total, num) => {
            return total + num.price;
        }, 0);
    }
    return calcPrice;
};

Hamburger.prototype.calculateCalories = function () {
    let calcCalories = this._size.calories + this._stuffing.calories;
    if (this._toppings.length > 0) {
        calcCalories += this._toppings.reduce((total, num) => {
            return total + num.calories;
        }, 0);
    }
    return calcCalories;
};

function HamburgerExceptions (message) {
    this.message = message;
    console.log('HamburgerException message:', message);
}


const hamburger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_POTATO);
const hamburger1 = new Hamburger();
hamburger.addTopping(Hamburger.TOPPING_MAYO);
console.log(hamburger.calculateCalories());
console.log(hamburger.calculatePrice());
hamburger.addTopping(Hamburger.TOPPING_SPICE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
console.log(hamburger.calculatePrice());
hamburger.removeTopping(Hamburger.TOPPING_MAYO);
console.log(hamburger.getToppings().length);
