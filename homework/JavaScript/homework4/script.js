/*Методы объекта это действия, которые можно выполнить с объектом. Это функции, которые храняться как свойства объекта*/


 function createNewUser() {
    let firstName = prompt('Enter your first name');
    let lastName = prompt('Enter your last name');

    let newUser = {
        firstName: firstName,
        lastName: lastName,
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        }
    };

    return newUser;
}
const user = createNewUser();

console.log(user.getLogin());