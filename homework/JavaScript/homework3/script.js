/*1. При программировании очень часто нужно повторять одно и тоже действие в разных частях программы. Для того, чтобы постоянно не дублировать код
используют функции. Функции - это основной строительный блок языка программирования, это тип данных, который относится к объекту. Они позволяют сократить код
при программировании, к  тому же позводляют совершать разные действия с объектами.
2. Аргументы функции - это локальные переменные в теле функции, это параметры, которые передаються при вызове функции, то есть можно сказать что,
это те параметры к которым функция будет применяться*/

let number1 = +prompt("Please, enter first number");
let number2 = +prompt("Please, enter second number");
let operation = prompt('Enter sign of mathematical operation');

/*На всякий случай оставляю код из if:
 if (operation==='+') {
    console.log(number1+number2);
} else if (operation==='-') {
    console.log(number1-number2);}
else if (operation==='*') {
    console.log(number1*number2);}
else if (operation==='/') {
    console.log(number1+number2);*/

function calculate(num1, num2, operation) {
    switch (operation) {
        case '+':
            console.log(num1 + num2);
            break;
        case '-':
            console.log(num1 - num2);
            break;
        case '/':
            console.log(num1 / num2);
            break;
        case '*':
            console.log(num1 * num2);
            break;
    }
}

const result = calculate(number1, number2, operation);
