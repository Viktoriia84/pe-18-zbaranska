/*1. Метод setTimeout позволяет вызвать функцию только один раз с указанной задержкой во времени, а метод setInterval - вызывает функицю регулярно через заданый
интервал времени/
2. Функиця сработает мгновенно (сразу после выполнения текущего кода). Минимальная задержка таймера браузера от 0 до 4мс(в зависимости от браузера).
3. Потому, что функция вызванная методом setInterval будет выолняться бесконечно.
 */

const images = ["./banns/1.jpg", "./banns/2.jpg", "./banns/3.jpg", "./banns/4.png"];
let i = 0;
const timerIndicator = document.getElementById("timer");
let downloadTimer;

function displayNextImage() {
    if (i < images.length - 1) {
        i++
    } else {
        i = 0;
    }
    document.getElementById("img").src = images[i];
}

document.getElementById('stop').addEventListener('click', function () {
    clearInterval(downloadTimer);
});

document.getElementById('resume').addEventListener('click', function () {
    counter()

});

function counter() {
    let timer = 10;
    timerIndicator.innerText = timer;
    downloadTimer = setInterval(function () {
        console.log(timer);
        timer--;
        timerIndicator.innerText = timer;
        if (timer === 0) {
            clearInterval(downloadTimer);
            displayNextImage();
            counter();
        }
    }, 1000);


}

counter();

