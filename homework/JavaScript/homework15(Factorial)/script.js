/*Рекурсия - это функция, которая вызывает сама себя. Обычно ее используют, когда можно привести функцию к более простому вызову,
а этот вызов - еще к более простому и так до тех пор , пока значение не станет очевидным*/

let userNumber = +prompt("Please, enter a number");
function factorial(userNumber) {
    return (userNumber !== 1) ? userNumber * factorial(userNumber - 1) : 1;
}

alert( factorial(userNumber));
