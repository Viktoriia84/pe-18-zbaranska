$(document).ready(function () {
    $(".nav-link").on("click", function (e) {
        e.preventDefault();
        let anchor = $(this);
        $('html, body').animate({
            scrollTop: $(anchor.attr('href')).offset().top
        }, 2000);
    });
});

$(window).scroll(function () {
    if ($(window).scrollTop() > 300) {
        $('#button').addClass('show');
    } else {
        $('#button').removeClass('show');
    }
});

$('#button').on('click', function (e) {
    $('html, body').animate({scrollTop: 0}, '4000');
});

$('#toggle').click(function () {
    $(".news").slideToggle("slow");
});