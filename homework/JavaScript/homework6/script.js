/*forEach - это метод, который испольуется для элементов массива, аналог цикла for. Для каждого элемента массива он вызывает функцию callback и передает ей три параметра
- очередной элемент массива, его индекс и название массива, который перебирается. Этот метод ничего не возвращает*/

function filterBy(array, dataType) {
    let newArray = array.filter(function(item) {
        return typeof (item) !== dataType ;
    });
    console.log(newArray);
    return newArray;
}
filterBy(['hello', 'world', 23, '23', null], 'string');
