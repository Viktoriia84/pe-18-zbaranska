/* Потому что человек может ввести значение в поле ввода без приминения клавиатуры , например через копировать - вставить. Таким образом события клавиатуры бесполезны
в данной ситуации (для инпутов).
*/

const buttons = document.getElementsByClassName('btn');

document.onkeydown = function (event) {
    console.log(event);

    [...buttons].forEach(function (btn) {
        console.log(btn);
        btn.classList.remove('blue');
        if (btn.innerText.toLowerCase() === event.key.toLowerCase() ) {
            btn.classList.add('blue');
        }
    });
};
