/*Экранирование используется в программирование, если нам нужно использовать специальные символы (например слеш \ или знак доллара $) как обычные, но эти сомиволы имеют
особенное(специальное) назначение в языке JS. Поэтому чтобы их можно было использовать как обычные, перед ними нужно постаить обратный косой слеш - это и есть
экранирование символов*/

function createNewUser() {
    let firstName = prompt('Enter your first name');
    let lastName = prompt('Enter your last name');
    let birthday = prompt("Write your birthday dd.mm.yy: ");

    let newUser = {
        firstName: firstName,
        lastName: lastName,
        birthday: birthday,

        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        getAge() {
            let now = new Date();
            let currentYear = now.getFullYear();
            let inputDate = +this.birthday.substring(0, 2);
            let inputMonth = +this.birthday.substring(3, 5);
            let inputYear = +this.birthday.substring(6, 10);
            let birthDate = new Date(inputYear, inputMonth - 1, inputDate);
            let birthYear = birthDate.getFullYear();
            let age = currentYear - birthYear;
            if (now < new Date(birthDate.setFullYear(currentYear))) {
                age = age - 1;
            }
            return age;
        },
        getPassword() {
            return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.substring(6, 10));

        }
    };

    return newUser;
}

const user = createNewUser();
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());