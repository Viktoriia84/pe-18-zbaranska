/*DOM - это модель построения документа в виде дереве из элементов (структуры), где все, что в ней находится представлено  в виде объектов. Эти объекты можно менять
 и с ними можно взаимодействовать*/

function showList(array) {
    let list = document.querySelector('#list');
    const liArray = array.map(function (item) {
        return `<li>${item}</li>`;
    });
    list.innerHTML = liArray.join('');
    console.log(liArray);
}

let i = 10;
let timerId = setInterval(function () {
    document.getElementById("timer").innerText = i;
    if (i === 0) {
        document.getElementById('list').classList.add("none");
        document.getElementById('timer').classList.add("none");
        clearInterval(timerId);
    }
    i--;
}, 1000);
showList(['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']);