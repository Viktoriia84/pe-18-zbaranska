const fruitShop = {
    items: {
        banana: 5,
        orange: 2,
        apple: 2,
        pineapple: 8,
        watermelon: 7,
        kiwi: 8,
        lemon: 4,
        melon: 0,
    },
    getFruit(fruitName, amount) {
        if (fruitName in this.items) {
            if (this.items[fruitName] < amount) {
                console.error('There are no so many fruits');
            } else {
                console.log('You can buy');
            }
        } else {
            console.error('No such fruit in our shop')
        }
    }
};
fruitShop.getFruit('apple', 1);