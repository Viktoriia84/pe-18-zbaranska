/*const myFunction= (...numbs) => {
    let maxNumber = Math.max(...numbs);
    return maxNumber;
};
const result = myFunction(2,15,30,4,48,1);*/

const maxNum = (...numbs) => {
    if (numbs.length ===0) {
        return "You didn't enter number"
    }
    let maxNumber = Number.NEGATIVE_INFINITY;
    for (let i of numbs) {
        if (i>maxNumber) {
            maxNumber = i;
        }
    }
    return maxNumber;
};
const result2 = maxNum(20,-10,89,4,16,-184);