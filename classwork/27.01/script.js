
function getUser (userName, userAge) {
    return {
        name: userName,
        age: userAge,
        increaseAge() {
            this.age++;
        },
        addField(key, value) {
            this[key] = value;
        }
    };
}
const user1 = getUser('John', 25);
user1.addField(key, value);
/*console.log(getUser('Ivan',25));
console.log(getUser.age);

function getCar() {
    let name = prompt("Enter name of the car");
    let model = prompt('Enter model of the car');
    let price = +prompt('Enter price');
    return {
        name,
        model,
        price,
    }
}
console.log(getCar());*/

