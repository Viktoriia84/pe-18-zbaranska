document.addEventListener('DOMContentLoaded', onDocumentLoaded);
function onDocumentLoaded() {
    const btn=document.createElement('button');
    const btnSend = document.getElementById('sendMessage');

    btn.setAttribute('type', 'button');
    btn.setAttribute('id', 'LabMouse');
    btn.innerText = 'Лабораторная мышка';
    btn.onclick = function() {
        console.log('Dynamic event!!!')
    };

    btnSend.parentNode.insertBefore(btn, btnSend);
    console.log('Test!!!!')
}
function onClientFormBtnClick () {
    const btnMouse = document.getElementById('LabMouse');
    btnMouse.addEventListener('click', function() {
        console.log('Покажи мне дату-> ', Date.now());
    });
    //alert('Hello');
}