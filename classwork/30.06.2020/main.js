//ES5
function Component(props) {
    this.props = props;
    this.container = this.createContainer();
}

Component.prototype.createContainer = function () {
    return document.createElement('div');
}
Component.prototype.appendTo = function (parent) {
    parent.append(this.container);
}
Component.prototype.destroy = function () {
    this.container.remove();
}

function Application(props) {
    //(props);
    Component.call(this,props);
    //Component.apply(this,[props]);- то же самое
    const list = new List({list: props.list});
    list.appendTo(this.container);
    const onRemove = (data) => {
        props = {
            ...props,
            list2: props.list2.filter(item => item !== data)
        };
        removableList.destroy();
        removableList = new RemovableList({list: props.list2, onRemove});
        removableList.appendTo(this.container);
    };
    let removableList = new RemovableList({list: props.list2, onRemove});
    removableList.appendTo(this.container);
}

Application.prototype = Object.create(Component.prototype);
//Component.prototype
/*
* appendTo()
* createContainer()
* destroy()
*
* createContainer()
* __proto__: {
*   appendTo()
*   createContainer()
*   destroy()
* }
*/

Application.prototype.createContainer = function () {
    const el = Component.prototype.createContainer.call(this);
    el.classList.add('app');
    return el;
}
function List (props) {
    Component.call(this.props);
    const {list} = props;
    const items = this.createItems(props.list)
    items.forEach(item => {
        item.appendTo(this.container);
    })
}
