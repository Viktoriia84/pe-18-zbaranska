const request = new XMLHttpRequest();
request.onreadystatechange = function (e) {
    if(request.readyState === 4 && request.status === 200)) {
        const data = JSON.parse(request.response);
        console.log(data)
    }
    if(request.status === 404) {
        alert("Data not found");
    }
}
request.send();
