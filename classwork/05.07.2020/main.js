const target = document.querySelector(".target");
target.addEventListener('drop', onDrop);
//target.addEventListener('dragenter', onDragenter);
target.addEventListener('dragover', onDragover);

function onDragStart(e) {
    e.dataTransfer.setData('text', 'source')
}

function onDragover (event) {
    event.preventDefault();
}

function onDrop(event) {
    event.preventDefault();
    if(event.dataTransfer.files.length) { 
    const reader = new FileReader();
    reader.readAsDataURL(event.dataTransfer.files[0]);
    reader.onload = () => {
        const img = document.createElement('img')
        img.src = reader.result;
        target.append(img);
    };
    return;
}
const selector = event.dataTransfer.getData('text');
target.append(document.querySelector(selector));

console.log('drop', event.dataTransfer.getData('text', 'source'));
}

