class Draggable {
    constructor(element, {distance = 0} = {}) {
        this._element = element;
        this._distance = distance;
        this._prepareStyles();
        this.enable();
    }

    enable() {
        this.enabled = true;
        this._onMousedownBound = this._onMousedown.bind(this);
        this._element.addEventListener('mousedown', this._onMousedownBound);
    }

    disable() {
        this.enabled = false;
        this._element.removeEventListener('mousedown', this._onMousedownBound);
    }

    _prepareStyles() {
        this._element.style.position = 'absolute';
    }


    _onMousedown({offsetX, offsetY, pageX, pageY}) {
       this._offsetX = offsetX;
       this._offsetY = offsetY;
       this._startX = pageX;
       this._startY = pageY;
       
       this._onMousemoveBound = this._onMousemove.bind(this);
       document.addEventListener('mousemove', this._onMousemoveBound);
       this._onMouseupBound = this._onMouseup.bind(this);
       document.addEventListener('mouseup', this._onMouseupBound);
    }

    _onMousemove({pageX, pageY}) {
        if(this._isDragging) {         
            const {top, left, bottom, right} = this._element.parentElement.getBoundingClientRect();
            let y = Math.max(pageY - this._offsetY, top);
            let x = Math.max(pageX - this._offsetX, left);
            y = Math.min(y, bottom - this._element.offsetHeight);
            x = Math.min(x, right - this._element.offsetWidth);
            this._element.style.left = `${x}px`;
            this._element.style.top = `${y}px`;
        }
        else {
            this._isDragging = Math.hypot(pageX - this._startX, pageY - this._startY >= this._distance);
        }
     }

    _onMouseup(event) {
        this._isDragging = false;
        document.removeEventListener('mousemove', this._onMousemoveBound);
        document.removeEventListener('mouseup', this._onMouseupBound);
    } 
}

const box = document.querySelector('.box');
const dnd = new Draggable(box, {distance:10});


const toggleDND = document.querySelector('button');
toggleDND.addEventListener('click', () => {
    if(dnd.enabled) {
        dnd.disable();
    }
    else {
        dnd.enable();
    }
})