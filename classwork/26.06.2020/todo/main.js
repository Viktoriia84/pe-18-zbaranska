const initialApplicationState = {
    list: ['item 1', 'item 2', 'abc'],
    list2: ['item 11', 'item 12', 'abcdef']
};

//ES6

class Component {
    constructor(props) {
        this.props = props
        this.container = this.createContainer();
    }
    createContainer() {
        //throw new Error('Write create container implementation');
        //console.warn('Write create container implementation');
        return document.createElement('div');
    }
    appendTo(parent) {
        parent.append(this.container);
    }
}

class Application extends Component {
    constructor(props) {
        super(props);
        //this.container.classList.add('app');
        const list = new List({list: props.list});
        list.appendTo(this.container);
        const onRemove = (data) => {
            props = {...props, list2: props.list2.filter(item => item!==data)};
        };
        const removableList = new RemovableList({list: props.list2, onRemove});
        removableList.appendTo(this.container);
    }
    createContainer() {
        const el = super.createContainer();
        el.classList.add('app');
        return el;
    }
}

class List extends Component {
    constructor(props) {
        super(props);
        const {list} = props;
        const items = this.createItems(props.list)
        items.forEach(item => {
            item.appendTo(this.container);
        })
    }
    createContainer() {
        const el = super.createContainer();
        el.classList.add('list');
        return el;
    }
    createItems(list) {
        return list.map(data => new ListItem({data}));
    }
}

class RemovableList extends List {
    createItems(list) {
        return list.map(data => new RemovableListItem({data, onRemove:this.props.onRemove}));
    }
}

class ListItem  extends Component{
    constructor(props) {
        super(props);
        this.container.innerText = props.data;
    }
    createContainer() {
        const el = super.createContainer();
        el.classList.add('list-item');
        return el;
    }
}

class RemovableListItem extends ListItem {
    constructor(props) {
        super(props);
        const button = this.createRemoveButton();
        this.container.append(button);
    }
    createRemoveButton() {
        const button = document.createElement('button');
        button.innerText = 'remove';
        button.addEventListener('click', this.remove.bind(this))
        return button;
    }
    remove() {
        this.container.remove();
        this.props.onRemove(this.props.data);
    }
}

const app = new Application(initialApplicationState);
app.appendTo(document.body);

