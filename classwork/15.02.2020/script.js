document.addEventListener('DOMLoadedContent', onReady);

function onReady () {
const token = localStorage.getItem('security-token');
document.getElementsByClassName('tab-container').item(0).innerHTML = token;


}

function  setStorageValue() {
 localStorage.setItem('security-token', Date.now());
}

function handleUlClick(event, el) {
    const target = event.target;

    if (target.tagName !=='LI') {
        return;
    }
    const id = target.dataset.id;

    for (let li of el.children) {
        li.classList.remove('active')
    }
}